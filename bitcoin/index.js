const axios = require("axios");
const bitcore = require("bitcore-lib");
const bip32 = require('bip32');
const bip39 = require('bip39');
const bitcoin = require('bitcoinjs-lib');
const fs = require('fs');
const pathToDB = './db.json';

// sochain_network = "BTC" mainnet bitcoin
const sochain_network = "BTCTEST";


const createAccount = async () => {
  //Define the network
  //use networks.bitcoin for mainnet
  const network = bitcoin.networks.testnet //use networks.testnet for testnet

  // Derivation path
  // Use m/49'/0'/0'/0 for mainnet
  const path = `m/49'/1'/0'/0` // Use m/49'/1'/0'/0 for testnet

  let mnemonic = bip39.generateMnemonic()
  const seed = bip39.mnemonicToSeedSync(mnemonic)
  let root = bip32.fromSeed(seed, network)

  let account = root.derivePath(path)
  let node = account.derive(0).derive(0)

  let btcAddress = bitcoin.payments.p2pkh({
    pubkey: node.publicKey,
    network: network,
  }).address

  if (fs.existsSync(pathToDB)) {
    // pathToDB exists
    console.log('exists:', pathToDB);
    fs.readFile('db.json', 'utf8', function readFileCallback(err, data){
        if (err){
            console.log(err);
        } else {
        db = JSON.parse(data); //now it an object
        db.table.push({ id: Date.now(), address: btcAddress, pk: node.toWIF(), balance: 0, mnemonic: mnemonic }); //add some data
        json = JSON.stringify(db); //convert it back to json
        fs.writeFile('db.json', json, 'utf8', (err) => {
            if (err) throw err;
            console.log('The file has been saved!');
        }); // write it back 
    }});
  } else {
      console.log("DOES NOT exist:", pathToDB);
      const db = {
          table: []
      };
      db.table.push({ id: Date.now(), address: btcAddress, pk: node.toWIF(), balance: 0, mnemonic: mnemonic });
      const json = JSON.stringify(db);
      fs.writeFile('db.json', json, 'utf8', (err) => {
          if (err) throw err;
          console.log('The file has been saved!');
      });
  }

  console.log(`
  Wallet generated:
  - Address  : ${btcAddress},
  - Key : ${node.toWIF()}, 
  - Mnemonic : ${mnemonic}
      
  `)
  return btcAddress;
}

const withdraw = async (amountToSend, recieverAddress) => {
    const privateKey = "cNnLt8dPkL1Xp111Z8o6ECV4kFRhQnLRGsQqkNQ7yWDjxk4ukFzP";
    const sourceAddress = "mzw6BSW8xDJtooVyVUH94vyNHHCZJUMSdx";
    const satoshiToSend = amountToSend * 100000000;
    let fee = 0;
    let inputCount = 0;
    let outputCount = 2;
    const utxos = await axios.get(
      `https://sochain.com/api/v2/get_tx_unspent/${sochain_network}/${sourceAddress}`
    );
    const transaction = new bitcore.Transaction();
    let totalAmountAvailable = 0;
  
    let inputs = [];

    utxos.data.data.txs.forEach(async (element) => {
      let utxo = {};
      utxo.satoshis = Math.floor(Number(element.value) * 100000000);
      utxo.script = element.script_hex;
      utxo.address = utxos.data.data.address;
      utxo.txId = element.txid;
      utxo.outputIndex = element.output_no;
      totalAmountAvailable += utxo.satoshis;
      inputCount += 1;
      inputs.push(utxo);
    });

    transactionSize = inputCount * 146 + outputCount * 34 + 10 - inputCount;
    // Check if we have enough funds to cover the transaction and the fees assuming we want to pay 20 satoshis per byte
  
    fee = transactionSize * 20;
    if (totalAmountAvailable - satoshiToSend - fee  < 0) {
      throw new Error("Balance is too low for this transaction");
    }

    inputs = inputs.sort(function(a, b) { 
      return ( a.satoshis < b.satoshis ? 1 : (a.satoshis > b.satoshis ? -1 : 0 ) );
    });

    let bestInputs = [];
    let satoshiInputsCount = 0;
    for (let i = 0; i < inputs.length; i++) {
      if (satoshiInputsCount >= satoshiToSend + fee) {
        break;
      }
      else {
        satoshiInputsCount += inputs[i].satoshis;
        bestInputs.push(inputs[i]);
      }
    }

    //Set transaction input
    // transaction.from(inputs);
    transaction.from(bestInputs);

    if (!recieverAddress) {
      recieverAddress = await createAccount();
    }
  
    // set the recieving address and the amount to send
    transaction.to(recieverAddress, satoshiToSend);
  
    // Set change address - Address to receive the left over funds after transfer
    transaction.change(sourceAddress);
  
    //manually set transaction fees: 20 satoshis per byte
    transaction.fee(fee);
  
    // Sign transaction with your private key
    transaction.sign(privateKey);
  
    // serialize Transactions
    const serializedTransaction = transaction.serialize();
    // Send transaction
    const result = await axios({
      method: "POST",
      url: `https://sochain.com/api/v2/send_tx/${sochain_network}`,
      data: {
        tx_hex: serializedTransaction,
      },
    });
    return result.data.data;
};

const deposit = async (amountToSend, sourceAddress, privateKey) => {
  const recieverAddress = "mzw6BSW8xDJtooVyVUH94vyNHHCZJUMSdx";
  const satoshiToSend = amountToSend * 100000000;
  let fee = 0;
  let inputCount = 0;
  let outputCount = 2;
  const utxos = await axios.get(
    `https://sochain.com/api/v2/get_tx_unspent/${sochain_network}/${sourceAddress}`
  );
  const transaction = new bitcore.Transaction();
  let totalAmountAvailable = 0;

  let inputs = [];

  utxos.data.data.txs.forEach(async (element) => {
    let utxo = {};
    utxo.satoshis = Math.floor(Number(element.value) * 100000000);
    utxo.script = element.script_hex;
    utxo.address = utxos.data.data.address;
    utxo.txId = element.txid;
    utxo.outputIndex = element.output_no;
    totalAmountAvailable += utxo.satoshis;
    inputCount += 1;
    inputs.push(utxo);
  });

  transactionSize = inputCount * 146 + outputCount * 34 + 10 - inputCount;
  // Check if we have enough funds to cover the transaction and the fees assuming we want to pay 20 satoshis per byte

  fee = transactionSize * 20;
  if (totalAmountAvailable - satoshiToSend - fee  < 0) {
    throw new Error("Balance is too low for this transaction");
  }

  inputs = inputs.sort(function(a, b) { 
    return ( a.satoshis < b.satoshis ? 1 : (a.satoshis > b.satoshis ? -1 : 0 ) );
  });

  let bestInputs = [];
  let satoshiInputsCount = 0;
  for (let i = 0; i < inputs.length; i++) {
    if (satoshiInputsCount >= satoshiToSend + fee) {
      break;
    }
    else {
      satoshiInputsCount += inputs[i].satoshis;
      bestInputs.push(inputs[i]);
    }
  }

  //Set transaction input
  // transaction.from(inputs);
  transaction.from(bestInputs);

  // set the recieving address and the amount to send
  transaction.to(recieverAddress, satoshiToSend);

  // Set change address - Address to receive the left over funds after transfer
  transaction.change(sourceAddress);

  //manually set transaction fees: 20 satoshis per byte
  transaction.fee(fee);

  // Sign transaction with your private key
  transaction.sign(privateKey);

  // serialize Transactions
  const serializedTransaction = transaction.serialize();
  // Send transaction
  const result = await axios({
    method: "POST",
    url: `https://sochain.com/api/v2/send_tx/${sochain_network}`,
    data: {
      tx_hex: serializedTransaction,
    },
  });
  return result.data.data;
};

(async () => {
    // const test = await withdraw('0.000006', 'mn9A7yTDdwa47CRtf9ty63pRc2ewJbXgCA');
    // console.log(test);
    await createAccount();
})();