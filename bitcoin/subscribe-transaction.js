const bitcoin = require('bitcoinjs-lib');
const zmq = require('zeromq');
const axios = require('axios');
const sock = zmq.socket('sub');
const addr = 'tcp://3.66.43.50:3000';
const uname = 'btcuser';
const pass = 'btcpass';
const nodeUrl = 'http://3.66.43.50:8332';
const dbEmulate = '';
sock.connect(addr);
sock.subscribe('rawtx');
sock.on('message', async function(topic, message) {
    // console.log(topic, message);
    if (topic.toString() === 'rawtx') {
        var rawTx = message.toString('hex');
        var tx = bitcoin.Transaction.fromHex(rawTx);
        var txid = tx.getId();
        // tx.ins = tx.ins.map(function(in) {
        //     in.address = bitcoin.address.fromOutputScript(in.script, bitcoin.networks.bitcoin);
        //     return in;
        // });
        // tx.outs = tx.outs.map(function(out) {
        //     out.address = bitcoin.address.fromOutputScript(out.script, bitcoin.networks.bitcoin);
        //     return out;
        // });
        console.log('received transaction', txid);
        if (!tx.isCoinbase()) {
            for (let out of tx.outs) {
                let address
                try {
                  address = bitcoin.address.fromOutputScript(out.script)
                } catch (e) {}
              
                if (address) {
                    console.log('to', address);
                    await getConfirmations(txid);
                }
            }
        }
    }
});

async function getConfirmations(txid) {
    const dataForRequest = {
        "jsonrpc": "1.0",
        "id": "curltest",
        "method": "getrawtransaction",
        "params": [txid, true]
    }
    await axios.post(nodeUrl, dataForRequest, {
        auth: {
          username: uname,
          password: pass
        }
    })
    .then(async function(response) {
        console.log('Authenticated');
        const confirmations = response.data.result.confirmations;
        if (confirmations < 3) {
            await sleep(60000);
            await getConfirmations(txid);
        } else {
            console.log('Transaction passed', confirmations);
            await sleep(30000);
        }
    })
    .catch(function(error) {
        console.log('Error on Authentication');
        error.message;
        throw Error ('Catch error');
    });
    console.log('tttt');

};

async function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
};