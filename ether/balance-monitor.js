const axios = require('axios');
const fs = require('fs');
const ETHERSCAN_API_KEY = '8HMWU8NJHGEVRYSKZGZDGCJ441HZUJK1HH'

const path = './db.json';

(async () => {
    if (fs.existsSync(path)) {
        // path exists
        console.log('exists:', path);
        fs.readFile('db.json', 'utf8', function readFileCallback(err, data) {
            if (err){
                console.log(err);
            } else {
            db = JSON.parse(data).table; //now it an object
            let urlAddresses = '';
            for (let i = 0; i < db.length; i++) {
                urlAddresses = urlAddresses.concat(db[i].address, i == db.length-1 ? '' : ',');
            }
            // Make a request
            const requestUrl = `https://api.etherscan.io/api?module=account&action=balancemulti&address=${urlAddresses}&tag=latest&apikey=${ETHERSCAN_API_KEY}`
            axios.get(requestUrl)
                .then(function (response) {
                    // handle success
                    console.log(response.data);
                })
                .catch(function (error) {
                    // handle error
                    console.log(error);
                })
        }});
    } else {
        console.log('No data for parse');
    }
})();