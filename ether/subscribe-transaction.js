const ethers = require('ethers');
const url = 'wss://ropsten.infura.io/ws/v3/b7ed9cff4bec4abdb07bd155445494a4';
const dbEmulate = '0x3f88fC9379347b44DB45E484e5e4c5C308a0E611';
const customWsProvider = new ethers.providers.WebSocketProvider(url);


const init = function () {
  
  customWsProvider.on('pending', (tx) => {
    customWsProvider.getTransaction(tx).then(function (transaction) {
        if (transaction && transaction.to === dbEmulate) {
          customWsProvider.once(transaction.hash, async (transaction) => {
              // Emitted when the transaction has been mined
              await getConfirmations(transaction.transactionHash);
          })
          console.log(transaction);
        }
    });
  });

  customWsProvider._websocket.on('error', async () => {
    console.log(`Unable to connect to ${ep.subdomain} retrying in 3s...`);
    setTimeout(init, 3000);
  });
  customWsProvider._websocket.on('close', async (code) => {
    console.log(
      `Connection lost with code ${code}! Attempting reconnect in 3s...`
    );
    customWsProvider._websocket.terminate();
    setTimeout(init, 3000);
  });
};

async function getConfirmations(hash) {
  customWsProvider.once(hash, async (transaction) => {
    if (transaction.confirmations < 20) {
      console.log('transaction.confirmations', hash, transaction.confirmations);
      await sleep(60000);
      await getConfirmations(hash);
    } else {
      console.log('transaction passed', transaction.confirmations);
    }
  })
}

async function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

init();
