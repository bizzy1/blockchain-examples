const fs = require('fs');
// import ethers.js
const ethers = require('ethers')
// network: using the Rinkeby testnet
let network = 'https://mainnet.infura.io/v3/b7ed9cff4bec4abdb07bd155445494a4';
// provider: Infura or Etherscan will be automatically chosen
let provider = ethers.getDefaultProvider(network);
const gasPrice = 1000000000000;

const path = './db.json';


async function createAccount() {
    const account = await ethers.Wallet.createRandom();
    console.log(account.address);
    console.log(account.privateKey);
    if (fs.existsSync(path)) {
        // path exists
        console.log('exists:', path);
        fs.readFile('db.json', 'utf8', function readFileCallback(err, data){
            if (err){
                console.log(err);
            } else {
            db = JSON.parse(data); //now it an object
            db.table.push({ id: Date.now(), address: account.address, pk: account.privateKey, balance: 0 }); //add some data
            json = JSON.stringify(db); //convert it back to json
            fs.writeFile('db.json', json, 'utf8', (err) => {
                if (err) throw err;
                console.log('The file has been saved!');
            }); // write it back 
        }});
    } else {
        console.log("DOES NOT exist:", path);
        const db = {
            table: []
        };
        db.table.push({ id: Date.now(), address: account.address, pk: account.privateKey, balance: 0 });
        const json = JSON.stringify(db);
        fs.writeFile('db.json', json, 'utf8', (err) => {
            if (err) throw err;
            console.log('The file has been saved!');
        });
    }
    console.log(account);
    return account;
}

async function deposit(amountInEther, privateKey) {

    let wallet = new ethers.Wallet(privateKey, provider)
    // Адрес получателя, который получает эфир (наш кошелек)
    let receiverAddress = '0x3f88fC9379347b44DB45E484e5e4c5C308a0E611'
    // Create a transaction object
    let tx = {
        to: receiverAddress,
        // Convert currency unit from ether to wei
        value: ethers.utils.parseEther(amountInEther),
        // 1000000000000 wei ~ 0.005 USD
        gasPrice: gasPrice
    }
    console.log('tx', tx);
    // Send a transaction
    const createReceipt = await wallet.sendTransaction(tx);
    await createReceipt.wait();
    console.log(`Transaction successful: ${JSON.stringify(createReceipt)}`);
}

async function withdraw(amountInEther, receiverAddress) {
    // наш кошелек
    const privateKey = '432fa9afa78f6bf7125241f13c620ad7c47eb11b5f1e45b38442abf4dd3d3116';
    // новый кошелек
    let newAccount = null;

    let wallet = new ethers.Wallet(privateKey, provider)

    if (!receiverAddress) {
        newAccount = await createAccount();
        console.log('created new account:');
        console.log(newAccount.address);
        console.log(newAccount.privateKey);
        receiverAddress = newAccount.address;
    }
    // Create a transaction object
    let tx = {
        to: receiverAddress,
        // Convert currency unit from ether to wei
        value: ethers.utils.parseEther(amountInEther),
        // 1000000000000 wei ~ 0.005 USD
        gasPrice: gasPrice
    }
    console.log('tx', tx);
    // Send a transaction
    const createReceipt = await wallet.sendTransaction(tx);
    await createReceipt.wait();
    console.log(`Transaction successful: ${JSON.stringify(createReceipt)}`);
}

(async () => {
    await deposit('0.0002', '5fb92d6e98884f76de468fa3f6278f8807c48bebc13595d45af5bdc4da702133');
})()
